require 'rails_helper'
require 'support/factory_bot'

# Define User
FactoryBot.define do
  factory :user do
    name "John"
    country "Australia"
  end
end

# Define Tag
FactoryBot.define do
	factory :tag do
		name "Hair & Beauty"
		category "beauty,category"
	end
end

# Define Taggable
FactoryBot.define do
	factory :taggable do
		association :user
	  association :tag
	end
end

describe "Users API with Tags" do
  it 'sends a list of users with tags' do 
	 	# Due to association, creating :taggable will also creates :user & :tag
	  FactoryBot.create(:taggable)
    get '/api/v1/users'

    # test for the 200 status-code
    expect(response).to be_success

    # ensure that returned json is not nil
    expect(json).not_to eq(nil)

    # ensure returned json User data is same as DB records
    expect(json['users'][0]['name']).to eq('John')
    expect(json['users'][0]['country']).to eq('Australia')

    # ensure returned json Tag data is same as DB records
    expect(json['users'][0]['tags'][0]['name']).to eq('Hair & Beauty')
    expect(json['users'][0]['tags'][0]['category']).to eq('beauty,category')	    
  end
end

describe "Users API without Tags" do
  it 'sends a list of users without tags' do
	  FactoryBot.create(:user)
    get '/api/v1/users'

    # test for the 200 status-code
    expect(response).to be_success

    # ensure that returned json is not nil
    expect(json).not_to eq(nil)

    # ensure returned json User data is same as DB records
    expect(json['users'][0]['name']).to eq('John')
    expect(json['users'][0]['country']).to eq('Australia')

    # ensure that NO returned Tag data
    expect(json['users'][0]['tags'].length).to eq(0)  
  end
end
