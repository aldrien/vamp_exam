# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Importing Default Data from CSV files

require 'csv'

users = File.read(Rails.root.join('db', 'csv', 'users.csv'))
users = CSV.parse(users, :headers => true, :encoding => 'ISO-8859-1')
users.each do |row|
  User.create!(
  	id: row['id'],
	  name: row['name'],
	  country: row['country'],
	  created_at: row['created_at'],
	  updated_at: row['updated_at']
  )
end


tags = File.read(Rails.root.join('db', 'csv', 'tags.csv'))
tags = CSV.parse(tags, :headers => true, :encoding => 'ISO-8859-1')
tags.each do |row|
  Tag.create!(
  	id: row['id'],
	  name: row['name'],
	  category: row['category'],
	  created_at: row['created_at'],
	  updated_at: row['updated_at']
  )
end

taggables = File.read(Rails.root.join('db', 'csv', 'taggables.csv'))
taggables = CSV.parse(taggables, :headers => true, :encoding => 'ISO-8859-1')
taggables.each do |row|
  Taggable.create!(
	  id: row['id'],
	  user_id: row['user_id'],
	  tag_id: row['tag_id'],
	  created_at: row['created_at'],
	  updated_at: row['updated_at']
  )
end
