class CreateTaggables < ActiveRecord::Migration[5.1]
  def up
    create_table :taggables do |t|
      t.integer :user_id
      t.integer :tag_id

      t.timestamps
    end
  end

  def down
  	drop_table :taggables
	end
end
