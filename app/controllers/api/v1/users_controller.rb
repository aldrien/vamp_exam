class Api::V1::UsersController < ApplicationController
  def index
  	users = User.all
  	render json: {users: users}, include: 'tags'
  end
end
