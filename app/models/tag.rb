class Tag < ApplicationRecord
	has_many :taggables
	has_many :users, through: :taggables
end
