class User < ApplicationRecord
	has_many :taggables
	has_many :tags, through: :taggables, dependent: :destroy

  # Exclude info from json output.
  def as_json(options={})
  	options[:except] ||= [:created_at, :updated_at]
  	super(options)
  end
end
