# README

This README would normally document whatever steps are necessary to get the
application up and running.

## Local Set Up

* Go to application directory in Terminal
```cd app_dir```

* Install local gems and dependencies
```bundle install```

* Update Database configuration file (if neccessary)
```app_dir/config/database.yml```

* Set up local database (does db:create, db:schema:load, db:seed)
```rails db:setup```

* Run App locally
```rails server or rails s```

* Visit link for API
```http://localhost:3000/api/v1/users```

* Run RSPEC tests (open new tab from Terminal)

	For specific test:
	```rspec spec/requests/api/v1/users_spec.rb```

	For all tests:
	```rspec```

	Test files are located in:
	```app_dir/spec/```